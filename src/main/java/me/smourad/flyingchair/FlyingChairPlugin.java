package me.smourad.flyingchair;

import org.bukkit.plugin.java.JavaPlugin;

public final class FlyingChairPlugin extends JavaPlugin {

    public static FlyingChairPlugin INSTANCE; { INSTANCE = this; }

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new FlyingChair(), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
