package me.smourad.flyingchair;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlyingChair implements Listener {

    private final List<FallingBlock> chairs = new ArrayList<>();
    private final Map<FallingBlock, Chicken> vessels = new HashMap<>();

    public FlyingChair() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(
                FlyingChairPlugin.INSTANCE, ListenerPriority.NORMAL, PacketType.Play.Client.STEER_VEHICLE) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                // ServerboundPlayerInputPacket
                PacketContainer packet = event.getPacket();
                Player player = event.getPlayer();

                if (player.getVehicle() instanceof FallingBlock chair && chairs.contains(chair)) {
                    Chicken vessel = vessels.get(chair);

                    Location location = player.getEyeLocation();
                    Vector direction = location.getDirection();
                    Vector y = direction.clone().setZ(0).setX(0);
                    Vector ws = direction.clone().setY(0);
                    Vector ad = rotateVectorAroundY(ws.clone(), 270);

                    ws.multiply(packet.getFloat().read(1));
                    ad.multiply(packet.getFloat().read(0));
                    Vector velocity = ws.clone().add(ad);

                    double length = velocity.length();
                    velocity.add(y).normalize().multiply(length);
                    vessel.setVelocity(velocity);
                }

            }
        });
    }

    protected Vector rotateVectorAroundY(Vector vector, double degrees) {
        double rad = Math.toRadians(degrees);

        double x = vector.getX();
        double z = vector.getZ();

        double cos = Math.cos(rad);
        double sin = Math.sin(rad);

        return new Vector((cos * x - sin * z), vector.getY(), (sin * x + cos * z));
    }

    public void spawn(Player player) {
        World world = player.getWorld();
        FallingBlock chair = world.spawnFallingBlock(player.getEyeLocation(), Material.ACACIA_STAIRS.createBlockData());
        chair.setSilent(true);
        chair.setPersistent(true);
        chair.setInvulnerable(true);
        chair.setGravity(false);

        new BukkitRunnable() {
            @Override
            public void run() {
                chair.setTicksLived(1);
            }
        }.runTaskTimer(FlyingChairPlugin.INSTANCE, 0L, 2L);

        Chicken vessel = (Chicken) world.spawnEntity(player.getEyeLocation(), EntityType.CHICKEN);
        vessel.setSilent(true);
        vessel.setPersistent(true);
        vessel.setInvulnerable(true);
        vessel.setGravity(false);
        vessel.setInvisible(true);

        chairs.add(chair);
        vessel.addPassenger(chair);
        vessels.put(chair, vessel);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        spawn(player);
    }

    @EventHandler
    public void onRightClick(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();

        if (event.getRightClicked() instanceof FallingBlock chair && chairs.contains(chair)) {
            if (chair.getPassengers().size() < 1) {
                chair.addPassenger(player);
            }
        }
    }

}
